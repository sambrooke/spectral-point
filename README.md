Current version:
[https://code.earthengine.google.com/df4aa7a259f743e0dfe9b0f32ef4b11d](https://code.earthengine.google.com/fe49cdb75de17f79aee8f8639ea7a495)
_________________
![Spectral point showcase](/showcase/showcase.jpg)

A Google Earth Engine app for rapid point spectral signature selection from
multispectral imagery

[Documentation](docs/spectral-point-readme_main.pdf)
